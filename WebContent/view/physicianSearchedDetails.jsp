<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body {
		
		
	background-image:
		url('https://images.unsplash.com/photo-1546659934-038aab8f3f3b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2017&q=80');
	
	background-size: 1600px 800px;
}
center {
	margin: auto;
	width: 50%;
}
</style>
</head>
<body>
<marquee direction="right" behavior="alternate" scrollamount="20">
<h1>YOU GOT IT! BUDDY</h1>
</marquee>
<br>
<br>
<br>
<br>
<table border="1" align="center" style="color:blue;background-color:#f59595;">

	<tr>
	<th>Physician ID</th>
	<th>Physician First Name</th>
	<th>Physician Last Name</th>
	<th>Department</th>
	<th>Educational Qualification</th>
	<th>Experience</th>
	<th>State</th>
	<th>Insurance Plan</th>
	</tr>
	<c:forEach items="${p}" var="p">
	<tr> 
	<td>${p.getPhysicianId()}</td>
	<td>${p.getPhysicianFirstName()}</td>
	<td>${p.getPhysicianLastName()}</td>
	<td>${p.getDepartment()}</td>
	<td>${p.getEducationalQualification()}</td>
	<td>${p.getYearsOfExperience()}</td>
	<td>${p.getState()}</td>
	<td>${p.getInsurancePlan()}</td>
	</tr>
	</c:forEach>
</table>
</body>
</html>