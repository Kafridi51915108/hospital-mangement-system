<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Details</title>
<style type="text/css">
body {
	background-image:
		url('http://www.imperialcrs.com/blog/wp-content/uploads/2015/09/photodune-3418930-leader-s.jpg');
	background-size: cover;
}

center {
	margin: auto;
	width: 50%;
}
</style>
</head>
<body>
	<marquee behavior="alternate" scrollamount="20">
		<h1>ENROLL PATIENT DETAILS</h1>
	</marquee>

	<c:form action="enroll" method="post" modelAttribute="ep">
		<hr>
		<p align="right">
			<a href="index.jsp"><button
					style="background-color: #ffb399; width: 16.6%">
					<b>LOGOUT</b>
				</button></a>
		</p>
		<table  align="left" border="1"style="background-color: #b9eba0;" >
			
			
			<tr>
				<td><label>Patient Id:</label></td>
				<td><c:input path="patientId" autocomplete="on" /></td>
			</tr>
			<tr>
				<td><label>Full Name :</label></td>
				<td><c:input path="fullName" autocomplete="on" /></td>
			</tr>
			<tr>
				<td><label>Last Name :</label></td>
				<td><c:input path="lastName" autocomplete="on" /></td>

			</tr>

			<tr>
				<td><label>Password</label></td>
				<td><c:input path="password" autocomplete="on" /></td>

			</tr>

			<tr>
				<td><label>Date Of Birth</label></td>
				<td><c:input path="dob" autocomplete="on" /></td>

			</tr>

			<tr>
				<td><label>Email Adreess</label></td>
				<td><c:input path="emailAdreess" autocomplete="on" /></td>

			</tr>

			<tr>
				<td><label> Contact Number</label></td>
				<td><c:input path="contactNumber" autocomplete="on" /></td>

			</tr>
			<tr>
				<td><label>State</label></td>
				<td><c:input path="state" autocomplete="on" /></td>

			</tr>
			<tr>
				<td><label> Insurance Plan</label></td>
				<td><c:input path="insurance" autocomplete="on" /></td>

			</tr>
			<tr>
				<td align="center" style="background-color:black;"><c:button>Enroll</c:button></td>
			</tr>
		</table>
	</c:form>
</body>
</html>