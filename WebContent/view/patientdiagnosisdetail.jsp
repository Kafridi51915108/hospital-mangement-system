<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Diagnosis Detail</title>
<style type="text/css">
body {
	background-image:
		url('https://images.unsplash.com/photo-1589279003513-467d320f47eb?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80');
	background-size: cover;
}

center {
	margin: auto;
	width: 50%;
}
</style>
</head>
<body>
	<marquee behavior="alternate" scrollamount="20">
		<h1>INSERT PATIENT DIAGNOSIS DETAILS</h1>
	</marquee>

	<hr>
	<s:form action="keys" method="post" modelAttribute="dd">
		<p align="right">
			<a href="index.jsp"><button
					style="background-color: #ffb399; width: 16.6%">
					<b>Logout</b>
				</button></a>
		</p>
		<table>
			<tr>
				<td><label>Patient Id:</label></td>
				<td><s:input path="patientId" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Diagnosis Id:</label></td>
				<td><s:input path="diagnosisId" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Symptoms:</label></td>
				<td><s:input path="symptoms" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Diagnosis Provided:</label></td>
				<td><s:input path="diagnosisProvided" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Administrated By:</label></td>
				<td><s:input path="adminstratedBy" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Date of Diagnosis</label></td>
				<td><s:input path="dateofDiagnosis" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Follow-Up Required</label></td>
				<td><s:input path="followupRequired" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Date Of Follow-Up</label></td>
				<td><s:input path="dateOfFollowUp" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Bill Amount</label></td>
				<td><s:input path="billAmount" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Card Number</label></td>
				<td><s:input path="cardNumber" autocomplete="off" /></td>

			</tr>
			<tr>
				<td><label>Mode of payment</label></td>
				<td><s:input path="modeofPayment" autocomplete="off" /></td>

			</tr>
			<tr>
				<td style="background-color: #ffb399;"align="center"><input type="submit" value="Add detail" /></td>
			</tr>

		</table>
	</s:form>
</body>
</html>