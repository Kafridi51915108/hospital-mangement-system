package dao;

import java.util.List;

import model.Addphysician;
import model.Admin;
import model.EnrollPatient;
import model.PatientDiagnosisDetail;

public interface AdminDao {

	public boolean adminAuth(Admin admin);
	public void enrollpatient(EnrollPatient ep);
 	public void addphysician(Addphysician p);
	public void save(PatientDiagnosisDetail dd);
	public  List<Addphysician> searchPhysician(Addphysician ad);
	public List<EnrollPatient> viewPatientHistory();
	
	
}
