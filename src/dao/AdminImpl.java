package dao;

import java.util.ArrayList;
import java.util.List;



import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.Addphysician;
import model.Admin;
import model.EnrollPatient;
import model.PatientDiagnosisDetail;


public class AdminImpl implements AdminDao{
	
	
	@SuppressWarnings("rawtypes")
	@Override
	public boolean adminAuth(Admin admin) {
		
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		boolean isValid=false;
		Query query=session.createQuery("from Admin where uid=?1 and password=?2");
		query.setParameter(1,admin.getUid());
		query.setParameter(2, admin.getPassword());
		List list=query.list();
		if(list !=null && list.size()>0)
		{
			isValid=true;
		}
		return isValid ;
	}

	@Override
	public void enrollpatient(EnrollPatient ep) {
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		session.save(ep);
		tx.commit();
	}

	@Override
	public void addphysician(Addphysician p) {
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		session.save(p);
		tx.commit();
		
	}

	
	@Override
	public void save(PatientDiagnosisDetail dd) {
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		session.save(dd);
		tx.commit();
		
	}
	
	// search physician
	@SuppressWarnings({"rawtypes","unused","unchecked"})
	@Override
	public List<Addphysician> searchPhysician(Addphysician ad) {
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		Transaction tx=session.beginTransaction();
		List<Addphysician> list=new ArrayList<Addphysician>();
		Query q= session.createQuery("from Addphysician where state=?1 and insurancePlan=?2 and Department=?3");
		q.setParameter(1, ad.getState());
		q.setParameter(2, ad.getInsurancePlan());
		q.setParameter(3, ad.getDepartment());
		
		List<Addphysician> li=q.list();
		for(Addphysician s:li) {
			list.add(s);
		}
		
		return list;
	}
	
	
	
	//view patient history
	@SuppressWarnings({"rawtypes","unchecked"})
	
	@Override
	public List<EnrollPatient> viewPatientHistory() {
		Session session=new Configuration().configure("cfg.xml").buildSessionFactory().openSession();
		session.beginTransaction();
		List<EnrollPatient> list= new ArrayList<EnrollPatient>();
		
		Query q=session.createQuery("from EnrollPatient");
		
		List<EnrollPatient> li=q.list();
		for(EnrollPatient s:li) {
			list.add(s);
		}
		return list;
	}

	

	
}
