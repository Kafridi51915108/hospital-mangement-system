package model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class PatientDiagnosisDetail {
	@Id
	private int patientId;
	private int diagnosisId;
	private String symptoms;
	private String diagnosisProvided;
	private String adminstratedBy;
	private Date dateofDiagnosis;
	private String followupRequired;
	private Date dateOfFollowUp;
	private long billAmount;
	private long cardNumber;
	private String modeofPayment;

	public PatientDiagnosisDetail() {
		// TODO Auto-generated constructor stub
	}

	public PatientDiagnosisDetail(int patientId) {
		super();
		this.patientId = patientId;
	}

	public PatientDiagnosisDetail(int patientId, int diagnosisId, String symptoms, String diagnosisProvided,
			String adminstratedBy, Date dateofDiagnosis, String followupRequired, Date dateOfFollowUp, long billAmount,
			long cardNumber, String modeofPayment) {
		super();
		this.patientId = patientId;
		this.diagnosisId = diagnosisId;
		this.symptoms = symptoms;
		this.diagnosisProvided = diagnosisProvided;
		this.adminstratedBy = adminstratedBy;
		this.dateofDiagnosis = dateofDiagnosis;
		this.followupRequired = followupRequired;
		this.dateOfFollowUp = dateOfFollowUp;
		this.billAmount = billAmount;
		this.cardNumber = cardNumber;
		this.modeofPayment = modeofPayment;
	}

	public int getPatientId() {
		return patientId;
	}

	public void setPatientId(int patientId) {
		this.patientId = patientId;
	}

	public int getDiagnosisId() {
		return diagnosisId;
	}

	public void setDiagnosisId(int diagnosisId) {
		this.diagnosisId = diagnosisId;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getDiagnosisProvided() {
		return diagnosisProvided;
	}

	public void setDiagnosisProvided(String diagnosisProvided) {
		this.diagnosisProvided = diagnosisProvided;
	}

	public String getAdminstratedBy() {
		return adminstratedBy;
	}

	public void setAdminstratedBy(String adminstratedBy) {
		this.adminstratedBy = adminstratedBy;
	}

	public Date getDateofDiagnosis() {
		return dateofDiagnosis;
	}

	public void setDateofDiagnosis(Date dateofDiagnosis) {
		this.dateofDiagnosis = dateofDiagnosis;
	}

	public String getFollowupRequired() {
		return followupRequired;
	}

	public void setFollowupRequired(String followupRequired) {
		this.followupRequired = followupRequired;
	}

	public Date getDateOfFollowUp() {
		return dateOfFollowUp;
	}

	public void setDateOfFollowUp(Date dateOfFollowUp) {
		this.dateOfFollowUp = dateOfFollowUp;
	}

	public long getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(long billAmount) {
		this.billAmount = billAmount;
	}

	public long getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(long cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getModeofPayment() {
		return modeofPayment;
	}

	public void setModeofPayment(String modeofPayment) {
		this.modeofPayment = modeofPayment;
	}

}