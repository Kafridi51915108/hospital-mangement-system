package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Admin {
@Id
	private int uid;
	private String password;
	
	public Admin() {
		// TODO Auto-generated constructor stub
	}

	public Admin(int uid, String password) {
		super();
		this.uid = uid;
		this.password = password;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
