package model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
@Entity
@PrimaryKeyJoinColumn(name="patientId")
public class EnrollPatient extends PatientDiagnosisDetail {

	private String fullName;
	private String	lastName;
	private  Date dob;
	private String	password;
	private String	emailAdreess;
	private long contactNumber;
	private String	state;
	private String	insurance;

	public EnrollPatient() {
		// TODO Auto-generated constructor stub
	}

	public EnrollPatient(Integer patientId,String fullName, String lastName, Date dob, String password, String emailAdreess,
			long contactNumber, String state, String insurance) {
		super(patientId);
		this.fullName = fullName;
		this.lastName = lastName;
		this.dob = dob;
		this.password = password;
		this.emailAdreess = emailAdreess;
		this.contactNumber = contactNumber;
		this.state = state;
		this.insurance = insurance;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailAdreess() {
		return emailAdreess;
	}

	public void setEmailAdreess(String emailAdreess) {
		this.emailAdreess = emailAdreess;
	}

	public long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getInsurance() {
		return insurance;
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	
	
	
	
	



}