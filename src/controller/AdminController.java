package controller;


import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import dao.AdminDao;
import dao.AdminImpl;
import model.Addphysician;
import model.Admin;
import model.EnrollPatient;
import model.PatientDiagnosisDetail;
@Controller
public class AdminController {

	AdminDao dao=new AdminImpl();
	@RequestMapping("validate")
	public ModelAndView AdminLoginValid(@ModelAttribute("a") Admin admin) {
		boolean isValid=dao.adminAuth(admin);
		if(isValid) {
			return new ModelAndView("admin","a","login Successfully");
		}else {
			return new ModelAndView("index","a","Invalid Credentials");
		}
	}
	
	
	
	
				//enrollpatient
	
	
	@RequestMapping("enrollpatient")// for jsp page
	public ModelAndView enrollpatient() {
		return new ModelAndView("enrollpatient","ep",new EnrollPatient());
	}
	
	@RequestMapping("enroll")// for form action
	public ModelAndView add(@ModelAttribute("ep") EnrollPatient ep) {
		dao.enrollpatient(ep);
		return new ModelAndView("enrollpatient","ep",ep);
	}
	
	
	
	
	
					// add Physician
	
	@RequestMapping("addphysician")// for jsp page
	public ModelAndView addphysician() {
		return new ModelAndView("addphysician","p",new Addphysician() );
	}
	
	@RequestMapping("Insert")// for form action
	public ModelAndView add(@ModelAttribute("p") Addphysician p) {
		dao.addphysician(p);
		return new ModelAndView("addphysician","p",p);
	}
	

	
	
			//patient Diagnosis details
	
	@RequestMapping("patientdiagnosisdetail")
	public ModelAndView save() {
		return new ModelAndView("patientdiagnosisdetail","dd", new PatientDiagnosisDetail ());
	}
	
	@RequestMapping("keys")// for form action
	public ModelAndView add(@ModelAttribute("dd")PatientDiagnosisDetail dd ) {
		dao.save(dd);
		return new ModelAndView("patientdiagnosisdetail","dd",dd);
	}
	
	
		//Patient History
	
	@RequestMapping("viewpatienthistory")
	public ModelAndView viewPatientHistory()
	{
		List<EnrollPatient> list=dao.viewPatientHistory();
		return new   ModelAndView("viewpatienthistory","ep",list);
		
	}
	
	  //search physician
	
	@RequestMapping("physicianSearch")
	public ModelAndView searchPhysician(Addphysician ad)
	{
		List<Addphysician> list=dao.searchPhysician(ad);
		return new   ModelAndView("physicianSearch","p",list);
		
	}
	
	@RequestMapping("physicianSearchedDetails")
	public ModelAndView searchPhysician1(Addphysician ad)
	{
		List<Addphysician> list=dao.searchPhysician(ad);
		return new   ModelAndView("physicianSearchedDetails","p",list);
		
	}
	
}
