<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view Patient History</title>
<style type="text/css">
body {
	background-image:
		url('https://images.unsplash.com/photo-1517120026326-d87759a7b63b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80');
	background-size: cover;
}

center {
	margin: auto;
	width: 50%;
}
</style>
</head>
<body>
<marquee direction="right" behavior="alternate" scrollamount="20">
	<h1 >View All Patient History</h1>
	</marquee>
	<p align="right">
		<a href="index.jsp"><button
				style="background-color: #ffb399; width: 16.6%">
				<b>Logout</b>
			</button></a>
	</p>

		<table border="1" >
			<tr align="center" valign="middle">
				<th style="background-color:#edccdd">PATIENT ID:</th>
				<th style="background-color:#edccdd">FIRST NAME:</th>
				<th style="background-color:#edccdd">LAST NAME:</th>
			</tr>
			<c:forEach items="${ep}" var="p">
				<tr style="background-color:white">
				<td >${p.getPatientId()}</td>
				<td >${p.getFullName()}</td>
				<td >${p.getLastName()}</td>
				</tr>
			
			</c:forEach>
			
			
		</table>
</body>
</html>