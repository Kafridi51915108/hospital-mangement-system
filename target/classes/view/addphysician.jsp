<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert Physician</title>
<style type="text/css">
body {
	background-image:
		url('https://images.unsplash.com/photo-1527613426441-4da17471b66d?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=1035&q=80');
	background-size: cover;
}

center {
	margin: auto;
	width: 50%;
}
h1{
color:white;
}

td {
	background-color: pink;
	color: black;
}
</style>
</head>
<body>
	<marquee behavior="alternate" scrollamount="20">
		<h1>ADD PHYSICIAN</h1>
	</marquee>
	<hr>

	<s:form action="Insert" method="post" modelAttribute="p">
		<p align="right">
			<a href="index.jsp"><button
					style="background-color: #ffb399; width: 16.6%">
					<b>LOGOUT</b>
				</button></a>

		</p>
		<table border="1" style="with: 50%">
			<tr align="center" valign="middle">
				<td><label>Physician Id</label></td>
				<td><s:input path="physicianId" autocomplete="on"
						readonly="readonly" /></td>
			</tr>
			<tr align="center" valign="middle">
				<td><label>Physician First Name</label></td>

				<td><s:input path="physicianFirstName" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">
				<td><label>Physician Last Name</label></td>

				<td><s:input path="physicianLastName" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">
				<td><label>Department</label></td>

				<td><s:input path="department" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">
				<td><label>Educational Qualification</label></td>

				<td><s:input path="educationalQualification" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">
				<td><label>Years Of Experience</label></td>

				<td><s:input path="yearsOfExperience" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">
				<td><label>State</label></td>

				<td><s:input path="state" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">
				<td><label>Insurance Plan</label></td>

				<td><s:input path="insurancePlan" autocomplete="on" /></td>

			</tr>
			<tr align="center" valign="middle">

				<td><input type="submit" value="Submit"></td>

				<td><input type="reset" value="Reset"></td>
			</tr>

		</table>
	</s:form>


</body>
</html>