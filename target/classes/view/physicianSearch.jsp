<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search Physician</title>
<style type="text/css">
body {
	background-image:
		url('https://images.unsplash.com/photo-1613918108466-292b78a8ef95?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1055&q=80');
	background-size: cover;
}
center {
	margin: auto;
	width: 50%;
}
</style>
</head>

<body>
<marquee direction="right" behavior="alternate" scrollamount="20">
<h1>SEARCH FOR PHYSICIAN</h1>
</marquee>
<a href="index.jsp"><button style="background-color:#89dfe8;" align="center">Logout</button></a>
<hr>

<form action="physicianSearchedDetails" method="post" modelAttribute="p">
<br>
<br>
<br>

		<table border="1"  align="center"style="with: 50%">
			<tr align="center" valign="middle" style="background-color:#89dfe8;">
				<td><label>State:</label></td>
				
				<td><input type="text" name="state"  /></td>
			</tr>
			<tr align="center" valign="middle" style="background-color:#89dfe8;">
				<td><label>Insurance Plane:</label></td>
				
				<td><input type="text" name="insurancePlan"  /></td>
			</tr>
			<tr align="center" valign="middle" style="background-color:#89dfe8;">
				<td><label>Department:</label></td>
				
				<td><input type="text" name="department"  /></td>
			</tr>
			<tr align="center" valign="middle" style="background-color:#89dfe8;">
				
				<td><input type="submit" value="Search"  /></td>
				<td><input type="reset" value="Reset"  /></td>
			
			</tr>

	</table>
</form>
</body>
</html>